import { BaseCharacter } from "./engine/system/character.ts";
import { HitDie } from "./engine/system/features.ts/base.ts";

const char = new class extends BaseCharacter {
  constructor() {
    super();

    this.Features.AddFeature(new HitDie(this, 8));
  }
}();

console.log(char);
