export interface IStatValue {
  Value: number;
}

export interface IStatBlock {
  Str: IStatValue;
  Dex: IStatValue;
  Con: IStatValue;
  Int: IStatValue;
  Wis: IStatValue;
  Cha: IStatValue;
}

export interface IFeature {
  Name: string;
  IsSingleton: boolean;
}

export interface IFeatureContainer {
  AddFeature(feature: IFeature): void;
  DelFeature(feature: IFeature): void;
  DelFeatureByName(feature: string): void;
}

export interface ICharacter {
  Stats: IStatBlock;

  HPMax: number;
  HP: number;

  Features: IFeatureContainer;
}
