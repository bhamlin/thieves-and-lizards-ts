import { ICharacter, IStatBlock, IStatValue } from "../engine.ts";
import { FeatureContainer } from "./feature.ts";

class BoundedValue {
  constructor(
    public Value: number,
    public Max: number,
    public Min: number = 0,
  ) {}
}

class StatValue extends BoundedValue implements IStatValue {
  constructor(public Value: number = 8) {
    super(Value, 0, 100);
  }

  GetMod(): number {
    return Math.floor(this.Value / 2) - 5;
  }
}

class StatBlock implements IStatBlock {
  Str = new StatValue();
  Dex = new StatValue();
  Con = new StatValue();
  Int = new StatValue();
  Wis = new StatValue();
  Cha = new StatValue();
}

export abstract class BaseCharacter implements ICharacter {
  Stats = new StatBlock();

  HPMax = 0;
  HP = 0;

  Features = new FeatureContainer();
}
