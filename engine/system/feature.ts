import { IFeature, IFeatureContainer } from "../engine.ts";
import { BaseCharacter } from "./character.ts";

abstract class AbstractBaseFeature implements IFeature {
  constructor(
    public Name: string,
    public Parent: BaseCharacter,
    public IsSingleton = true,
  ) {}
}

export abstract class AbstractCharacterFeature extends AbstractBaseFeature {}
export abstract class AbstractSkill extends AbstractBaseFeature {}
export abstract class AbstractFeat extends AbstractBaseFeature {}

export class FeatureContainer implements IFeatureContainer {
  // Feature instance container
  private features: { [key: string]: IFeature } = {};
  // Expects only single instances of features.
  // Feature classes *_must_* implement tracking of multiples if supported.

  // Add provided feature instance to container
  AddFeature(feature: IFeature) {
    this.features[feature.Name] = feature;
  }

  DelFeature(feature: IFeature) {
    this.DelFeatureByName(feature.Name);
  }

  DelFeatureByName(feature: string) {
    delete this.features[feature];
  }
}
