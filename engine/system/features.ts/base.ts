import { AbstractCharacterFeature } from "../feature.ts";
import { BaseCharacter } from "../character.ts";

class HitDie extends AbstractCharacterFeature {
  constructor(public Parent: BaseCharacter, public Sides: number = 8) {
    super("HitDie", Parent);
  }
}

export { HitDie };
